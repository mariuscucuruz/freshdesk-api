<?php
/**
 * @author Marius Cucuruz
 * @copyright 2017
 * 
 * attempt to use the Freshdesk API
 * 
 * Note on ticket attachments:
 * The total size of these attachments cannot exceed 15MB!
 */
require_once ('freshdesk/config.php');
$tickets        = new FreshDesk($config);

# default action:
$api_action     = "tickets"; #contacts # #

# set filters
$api_filters    = [];
$api_filters[]  = ""; #"new_and_my_open";


/**
 * UPDATE EXISTING TICKET PROCEDURE:
 */
if ( isset($_POST['update_ticket']) && $_POST['ticket_id'] > 0)
{

    # check ticket exists:
    $ticketID           = $_POST['ticket_id'];
    $getTicket          = $tickets->getTicket($_POST['ticket_id']);

    # only perform update if ticket exists:
    if ($getTicket->results['http_code']['http_code'] == 200)
    {
        #echo (var_dump($_POST['conversation']));
        #die ("ticketID #". $ticketID ."!<pre>post:". print_r($_POST, true) ."</pre>");

        if (is_numeric(@$_POST['status']) && @$_POST['status'] > 0)
        {
            # change status:
            $status         = $_POST['status'];
            $result         = $tickets->updateStatus($status);
        }

        if (strlen(@$_POST['conversation']) > 1)
        {
            # add comment
            $conversation   = nl2br(htmlspecialchars(htmlentities($_POST['conversation'])));
            $result         = $tickets->addConversation($conversation);
        }

        ## NOT COMPLETED!!!
        if ($_FILES['attachment'] )
        {
            # add attachment
            $attachment     = $_FILES['attachment'];
            #$result         = $tickets->addAttachment($attachment);
        }

        #die ("<pre>ticketID #". $ticketID ."!result:". print_r($result, true) ."</pre>");
    }
    header('Location: '. $_SERVER['SCRIPT_NAME'] ."?ticket=$ticketID&result=". @$result['http_code'] ."&");
}

/**
 * SUBMIT NEW TICKET PROCEDURE:
 */
if ( isset($_POST['raise_ticket']))
{

    $attributes     = array();
    $name           = @$_POST['name'];
    $email          = @$_POST['email'];
    $subject        = @$_POST['subject'];
    $description    = @$_POST['description'];

    if (!$name || !$email || !$subject || !$description)
    {
        echo "Missing at least one of the mandatory fields (ie name, email, subject or description)!";
        return;
    }
    else {

        #die ("<pre>POST:". print_r($_POST, true) ."</pre>");

        $status         = (@$_POST['status']   > 0) ? $_POST['status']   : 3;
        $priority       = (@$_POST['priority'] > 0) ? $_POST['priority'] : 1;
        $source         = (@$_POST['source']   > 0) ? $_POST['source']   : 9;#2
        $description    = @nl2br(htmlspecialchars(htmlentities($_POST['description'])));
        $ticket_type    = @htmlspecialchars(htmlentities($_POST['type']));
        $cause          = @htmlspecialchars(htmlentities($_POST['cause']));

        # set attributes array to pass to addNewTicket: 
        $attributes  = [
                        'name'          => $name,
                        'email'         => $email,
                        'subject'       => $subject,
                        'description'   => $description,
                        'status'        => $status,
                        'priority'      => $priority,
                        'source'        => $source,
                        'type'          => $ticket_type,
                        'ticket_type'   => $ticket_type,
                        'cause'         => $cause,
                        'cc_emails'     => ['mariuscucuruz@gmail.com'],
                    ];
        $newTicket  = $tickets->addNewTicket($attributes);
        $newTicketData = $newTicket->results;
        #$ticketID   = $newTicket->results['data']['id'];
        die ("<pre>new ticket #". @$ticketID ." result:". print_r($newTicket->results['data'], true) ."</pre>");


        # get the new ticket to attach file(s):
        $ticketData = $tickets->getTicket($ticketID);
        if ($ticketData['http_code'] == 200)
        {
            ## NOT DONE YET!!!
            if ($_FILES['attachments'] )
            {
                # add attachment
                $attachments     = $_FILES['attachments'];
                #$result          = $tickets->addAttachment($attachments);
            }
        }
        else {
            echo "Error ". $ticketData['http_code'] ." occured! Ticket not created!";
            return;
        }
        #die ("<pre>ticketID #". $ticketID ." result:". print_r($ticketData, true) ."</pre>");
    }
    header('Location: '. $_SERVER['SCRIPT_NAME'] ."?ticket=$ticketID&result=". @$result['http_code'] ."&");
}


/**
 * SHOW TICKET DETAILS:
 */
if (@$_GET['ticket'] > 0)
{
    # fetch ticket data:
    $ticketData = $tickets->getTicket($_GET['ticket']);

    if ($ticketData->results['http_code'] == 200)
    {
        $ticketArr = $ticketData->results['data'];
        #die ("<pre>ticketArr:<br />". print_r($ticketData->showInfoCard(), true) ."</pre>");

        # list primary info:
        echo "<strong>Ticket #". @$ticketArr['id'] ."</strong>:<br />".
                "Subject: ".     $ticketArr['subject'] ."<br />".

                "Type: ".        $ticketArr['type'] ." (". @$ticketArr['ticket_type'] .")<br />".
                "Status: ".      $tickets::getTicketStatus($ticketArr['status']) .
                    ((@$ticketArr['deleted'] == 1) ? " (<strong>deleted</strong>)" : '') .
                    "<br />".
                "Escalated: ".   (@$ticketArr['is_escalated'] == 1 ? 'Yes' : 'No') ."<br />".

                "Source: ".      $tickets::getTicketSource($ticketArr['source']) ."<br />".
                "Requester: ".   $ticketArr['requester']['name'] ." (". $ticketArr['requester']['email'] .")<br />".
                "Agent: ".       @$ticketArr['agent'] ."<br />".
                "Sent to: ".     @implode(", ", $ticketArr['to_emails']) ."<br />".

                "Due by: ".      $tickets::pretifyDateTime($ticketArr['due_by']) ."<br />".
                "Created at: ".  $tickets::pretifyDateTime($ticketArr['created_at'], 'd/m/Y, H:i:s') ."<br />".
                "Updated at: ".  $tickets::pretifyDateTime($ticketArr['updated_at'], 'd/m/Y, H:i:s') ."<br />".
                "<br />";

        # list conversations / replys:
        if (is_array($ticketArr['conversations']) && count($ticketArr['conversations']) > 0)
        {
            echo "<ul>";
            foreach ($ticketArr['conversations'] as $ticket_conversation)
            {
                echo "<li style='font-family: sans-serif; font-size: 0.8em;'>".
                    "Reply @ ". $ticket_conversation['created_at'] .": ".
                    "<em>from <strong>". $ticket_conversation['from_email'] ."</strong> to ". implode(',', $ticket_conversation['to_emails']) .")</em><br />".
                    
                    "<div style='padding-left: 16px; margin: 10px 0; line-height: 1em;'>". nl2br($ticket_conversation['body_text']) ."</div>".
                    "<hr />".
                    "</li>";
            }
            echo "</ul>";
        }
?>
<div style="font-family: sans-serif;font-size: 16px;width: 550px;margin: 0 auto;">
    <form enctype="multipart/form-data" accept="image/gif, image/jpeg"  action="" method="POST" autocomplete="on">
        <fieldset style="border: 1px solid #818082; border-radius: 7px; box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2), 0 6px 20px 0 rgba(0, 0, 0, 0.19);">
            <legend><strong>Update Ticket</strong></legend>
            <br />

            <label for="conversation">Type Comment:</label><br />
            <textarea  name="conversation" id="conversation" rows="3" wrap="virtual" style="width: 100%;">test comment to ticket #<?=@$ticketArr['id'] ?> using API</textarea>
            <br /><br />

            <label for="status">Change Status:</label>
            <select name="status" id="status" style="width: 150px;">
                <option selected="selected">No change</option>
                <option value="5">Closed</option>
                <option value="4">Resolved</option>
                <option value="3">Pending</option>
                <option value="2">Open</option>
            </select>
            <br /><br />

            <label>Attach file:</label>
            <input type="file" id="attachment" name="attachment" multiple="multiple" value="select file" />
            <br /><br />

            <input type="submit" name="update_ticket" id="update_ticket" value="Submit" />
            <input type="hidden" name="ticket_id" id="ticket_id" value="<?=@$ticketArr['id'] ?>" />
        </fieldset>
    </form>
</div>

<?php
        #die ("<pre>ticket conversations:<br />". print_r($ticketArr['conversations'], true) ."</pre>");
    }
    else {
            echo "<h2>Error - HTTP Status Code : ". $ticketData->results['http_code'] ."</h2>\n";
    }

}

/**
 * LIST ALL TICKETS:
 */
else {
?>
<div style="font-family: sans-serif;font-size: 16px;width: 550px;margin: 0 auto;">
    <form enctype="multipart/form-data" accept="image/gif, image/jpeg"  action="" method="POST" autocomplete="on">
        <fieldset style="border: 1px solid #818082; border-radius: 7px; box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2), 0 6px 20px 0 rgba(0, 0, 0, 0.19);">
            <legend><strong>Raise New Ticket</strong></legend>
            <br />

            <label for="name">Name of requester</label><br />
            <input name="name" id="name" style="width: 100%;" value="Some Body" />
            <br /><br />

            <label for="email">Requester's email </label><br />
            <input name="email" id="email" style="width: 100%;" value="my.api@my-corp.com" />
            <br /><br />

            <label for="subject">Ticket subject:</label><br />
            <input name="subject" id="subject" style="width: 100%;" value="raising a ticket #<?=@$ticketArr['id'] ?> via API" />
            <br /><br />

            <label for="source">Ticket Type:</label><br />
            <select name="type" id="type" style="width: 150px;">
                <option value="IT" selected="selected">IT</option>
                <option value="Facilities">Facilities</option>
            </select>
            <br /><br />

            <label for="source">Ticket source:</label><br />
            <select name="source" id="source" style="width: 150px;">
                <option value="1">Email</option>
                <option value="2" selected="selected">Portal</option>
                <option value="3">Phone</option>
                <!--
                <option value="4">Forum</option>
                <option value="5">Twitter</option>
                <option value="6">Facebook</option>
                //-->
                <option value="7">Chat</option>
                <option value="8">MobiHelp</option>
                <option value="9">Feedback Widget</option>
            </select>
            <br /><br />

            <label for="cause">Resolution:</label><br />
            <select name="cause" id="cause" style="width: 150px;">
                <option value="0">Hardware</option>
                <option value="1">NFF</option>
                <option value="2">See Description</option>
                <option value="3">Software</option>
                <option value="4" selected="selected">Support Request</option>
                <option value="5">User</option>
            </select>
            <br /><br />

            <label for="priority">Ticket priority:</label><br />
            <select name="priority" id="priority" style="width: 150px;">
                <option value="1">Low</option>
                <option value="2" selected="selected">Medium</option>
                <option value="3">High</option>
                <option value="4">Urgent</option>
            </select>
            <br /><br />

            <label for="status">Ticket status:</label><br />
            <select name="status" id="status" style="width: 150px;">
                <option value="2">Open</option>
                <option value="3">Pending</option>
                <option value="4">Resolved</option>
                <option value="5">Closed</option>
            </select>
            <br /><br />

            <label for="description">Description:</label><br />
            <textarea  name="description" id="description" rows="3" wrap="virtual" style="width: 100%;">test ticket created by using the API</textarea>
            <br /><br />

            <label>Attach file:</label>
            <input type="file" id="attachment" name="attachment" multiple="multiple" value="select file" />
            <br /><br />

            <input type="submit" name="raise_ticket" id="raise_ticket" value="Submit" />
        </fieldset>
    </form>
</div>

<?php
    # set filter if needed:
    if (@$_GET['filter'] != '')
        $api_filters[]  = "filter=". urlencode($_GET['filter']); # new_and_my_open, watching, spam, deleted

    # set requester email filter:
    if (@$_GET['email'] != '')
        $api_filters[]  = "email=". urlencode($_GET['email']);


    #fetch all tickets:
    $all_tickets  = $tickets->getTickets($api_filters);
    #die ("<pre>ticketArr:<br />". print_r($all_tickets->results, true) ."</pre>");

    if ($all_tickets->results['http_code'] == 200)
    {
        foreach ($all_tickets->results['data'] as $ticketArr)
        {
            echo "<a href='?ticket=". @$ticketArr['id'] ."'>Ticket #". @$ticketArr['id'] ."</a>:<br />".
                    "Subject: ".     $ticketArr['subject'] ."<br />".
                    "Status: ".      $tickets::getTicketStatus($ticketArr['status']) .
                        ((@$ticketArr['deleted'] == 1) ? " (<strong>deleted</strong>)" : '') .
                        "<br />".

                    "Type: ".        $ticketArr['type'] ."<br />".
                    "Source: ".      $tickets::getTicketSource($ticketArr['source']) ."<br />".
                    "Escalated: ".   (@$ticketArr['is_escalated'] == 1 ? 'Yes' : 'No') ."<br />".
                    "Requester: ".   @$ticketArr['requester']['name'] ." (". @$ticketArr['requester']['email'] .")<br />".

                    "Sent to: ".     @implode(", ", $ticketArr['to_emails']) ."<br />".
                    "Due by: ".      $tickets::pretifyDateTime($ticketArr['due_by']) ."<br />".
                    "Created at: ".  $tickets::pretifyDateTime($ticketArr['created_at'], 'd/m/Y, H:i:s') ."<br />".
                    "Updated at: ".  $tickets::pretifyDateTime($ticketArr['updated_at'], 'd/m/Y, H:i:s') ."<br />".

                    "<br />";
            #die ("<pre>ticketArr:<br />". print_r($ticketArr, true) ."</pre>");
        }
    }
    else {
        echo "<h2>Error - HTTP Status Code : ". $all_tickets->results['http_code'] ."</h2>\n";
    }


}
require_once('freshdesk/debug.php');