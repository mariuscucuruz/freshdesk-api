<?php
# make sure the config is included
require_once ('config.php');

# instantiate object
$ticketz        = new FreshDesk($config);

# perform test
$api_response    = $ticketz->test_conn()->results;

# show debug info
echo "<br />---DEBUG INFO:--".
    "<h2>Debug info shown below:</h2>\n".
    "HTTP Status Code : ". @$api_response['http_code'] ."<br />\n".
    "<strong>Response Headers</strong> are:<br />\n".
    "<strong>Full JSON Response</strong>:<br />\n".
    "<pre>". @print_r($api_response['data'], true) ."</pre>\n".
    "<br /><hr /><br />".
    "<pre>info:". print_r($api_response, true) ."</pre>".
    "---END---";
