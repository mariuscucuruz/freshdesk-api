<?php
/**
 * @author Marius Cucuruz
 * @copyright 2017
 * 
 * attempt to use the WhosOff API
 * 
 * documentation regarding the API:
 * https://www.whosoff.com/features/api/
 * 
 * Endpoint: https://wr1.whosoff.com/api/
 * Available endpoints and descriptions:
 *   GET: /api/staff              #Retrieve staff details including tags and allowances
 *   GET: /api/department         #Retrieve department names
 *   GET: /api/tag                #Retrieve tag names
 *   GET: /api/free-restricted    #Retrieve free / restricted details
 *   GET: /api/leave-type         #Retrieve leave type names
 *   GET: /api/whosoff            #Retrieve leave details
 *   GET: /api/sandbox            #NO AUTHENTICATION REQUIRED  Returns Server Date Time, you IP address and Server Name
 *   GET: /api/sandbox-with-authentication    #As per /api/sandbox but authentication is required
 */

ini_set('max_execution_time', '600');
header('Cache-Control: no-cache, no-store, must-revalidate'); // HTTP 1.1.
header('Pragma: no-cache'); // HTTP 1.0.
header('Expires: 0');       // Proxies.
@session_start();

## API config array:
$config = [
        'api_key'       => "xxxxxxxxxx",
        'api_pass'      => "xxxxxxxxxx",
        'api_domain'    => "xxxxxxxxxx",
        ];
$config['api_url']      = "https://". $config['api_domain'] .".freshdesk.com/api/v2/";

require_once ('class.API.php');
