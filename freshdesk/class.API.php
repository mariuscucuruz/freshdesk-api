<?php
/**
 * @author Marius Cucuruz
 * @copyright 2017
 * 
 * attempt to use the Freshdesk API
 * 
 * Note on ticket attachments:
 * The total size of these attachments cannot exceed 15MB!
 * 
 * Official documentation:
 * https://developer.freshdesk.com/api/
 * 
 */

class FreshDesk {

    # config array:
    private $config;

    # cURL options:
    private $curl_options;

    # req url
    protected $api_url;

    # filtering options:
    private $filters;

    # array filled w/ results:
    public $results;

    /**
     * Constructor.
     *
     * @param    array    $config (auth_key & url)
     */
    public function __construct ($config = FALSE) {

        if (!$config && count($config) > 1)
            return "no configuration passed";

        $headers[]          = "Content-type: application/json";
        $this->api_url      = $config['api_url'];
        $this->filters      = [];
        $this->results      = [];

        # define cURL options:
        $this->curl_options   = array(
                        CURLOPT_HEADER              => true,
                        CURLOPT_HTTPHEADER          => $headers,
                        CURLOPT_URL                 => $this->api_url,
                        CURLOPT_USERPWD             => $config['api_key'] .":". $config['api_pass'],
                        CURLOPT_RETURNTRANSFER      => true,
                        CURLOPT_SSL_VERIFYHOST      => false,
                        CURLOPT_SSL_VERIFYPEER      => true,
                        CURLOPT_FRESH_CONNECT       => false, # try to cache
                        #CURLOPT_DNS_CACHE_TIMEOUT  => false, # seconds to cache DNS entries
                        CURLOPT_DNS_USE_GLOBAL_CACHE => false, # do NOT use a global DNS cache
                        CURLOPT_USERAGENT          => 'PHP cURL'
                        );

        return $this;
    }

    /**
     * Method to test connection to API server
     *
     * @param    string    $field (field name from customer table)
     */
    public function test_conn () {

        $this->curl_handle      = curl_init();
        $this->curl_options[CURLOPT_POST] = FALSE;
        $this->curl_options[CURLOPT_URL]  = $this->api_url .'ticket_fields';


        curl_setopt_array($this->curl_handle, $this->curl_options);

        $server_output          = curl_exec($this->curl_handle);
        $info                   = curl_getinfo($this->curl_handle);
        $header_size            = curl_getinfo($this->curl_handle, CURLINFO_HEADER_SIZE);
        $headers                = substr($server_output, 0, $header_size);
        $response               = substr($server_output, $header_size);
        $json_response          = json_decode($response, true);

        #echo "<pre>JSON response:". print_r($json_response, true) ."</pre>\n";
        #echo "<pre>HTTP Status Code: ". print_r($info, true) ."</pre>\n";

        $this->results          = $this->doRequest()->results;
        return $this;

        if ($info['http_code'] == 200)
        {
            return TRUE;
        }
        else {
            return $info['http_code'];
        }
    }

    /**
     * Method to perform cURL request to API server
     *
     * @param    none
     * @return   array      (headers, JSON string)
     */
    private function doRequest () {

        $this->curl_handle      = curl_init();
        curl_setopt_array($this->curl_handle, $this->curl_options);

        $server_output          = curl_exec($this->curl_handle);
        $info                   = curl_getinfo($this->curl_handle);
        $header_size            = curl_getinfo($this->curl_handle, CURLINFO_HEADER_SIZE);
        $headers                = substr($server_output, 0, $header_size);
        $response               = substr($server_output, $header_size);
        $json_response          = json_decode($response, true);

        # format response array with headers and data:
        $this->results          = array(
                                  'http_code' => $info['http_code'],
                                  'data'      => $json_response,
                                  );

        #if ($info['http_code'] > 201)
            die (""
                ."<pre>jSON response:". print_r($json_response, true) ."</pre> \n"
                ."<pre>server output:". print_r($info, true) ."</pre> \n"
                ."<pre>Ticket params:". print_r($this->results, true) ."</pre>"
                ."");

        return $this;
    }


    /**
     * Method to create new tickets
     * 
     *   Any of the five attributes is mandatory:
     *      requester_id    (number)
     *      facebook_id     (string)
     *      twitter_id      (string)
     *      email           (string)
     *      phone           (string)
     * 
     * @param    array      $paramsArr (auth_key & url)
     * @return   doRequest  result (JSON string)
     */
    public function addNewTicket ($paramsArr = array()) {

        if (!is_array($paramsArr) || !count($paramsArr) > 1)
            return false;

        unset($this->curl_options[CURLOPT_HTTPHEADER]);
        $this->curl_options[CURLOPT_HTTPHEADER]     = ['Content-type: application/json'];
        #$this->curl_options[CURLOPT_POST]           = TRUE;
        #$this->curl_options[CURLOPT_CUSTOMREQUEST]  = 'POST';

        if (strtolower(@$paramsArr['type']) == 'facilities')
        {
            $paramsArr['group']   = 'Facilities';
            $paramsArr['product'] = 'Fleet and Facilities'; #24000000197
            $paramsArr['type_it'] = '';
            $paramsArr['cause']   = '';
        }
        else if (strtolower(@$paramsArr['type']) == 'fleet')
        {
            $paramsArr['group']   = 'Fleet';
            $paramsArr['product'] = 'Fleet and Facilities'; #24000000197
            $paramsArr['type_it'] = '';
            $paramsArr['cause']   = '';
        }
        else {
            $paramsArr['group']   = 'Application & Development Support';
            $paramsArr['product'] = '';
            $paramsArr['type_it'] = 'Unknown Type';
            $paramsArr['cause']   = 'Hardware';
        }

        $this->curl_options[CURLOPT_POSTFIELDS]     = json_encode([
                                                        'name'          => @$paramsArr['name'],
                                                        'email'         => @$paramsArr['email'],
                                                        'subject'       => @$paramsArr['subject'],
                                                        'description'   => @$paramsArr['description'],
                                                        #'description_html' => strip_tags(@$paramsArr['description']),
                                                        'source'        => (int)$paramsArr['source'], #(@$paramsArr['source']   > 0) ? ($paramsArr['source'] * 1)   : 9,
                                                        'priority'      => (@$paramsArr['priority'] > 0) ? ($paramsArr['priority'] * 1) : 5,
                                                        'status'        => (@$paramsArr['status']*1),
                                                        'type'          => 'Facilities',#@$paramsArr['ticket_type'],

                                                        #'group'         => 'NFF',# @$paramsArr['cause'], #'Support Request',
                                                        #'product'       => 'NFF',# @$paramsArr['cause'], #'Support Request',
                                                        #'type_it'       => 'NFF',# @$paramsArr['cause'], #'Support Request',

                                                        'group_id'      => 6000198310,
                                                        'responder_id'  => 6005520783,#24002629463,
                                                        'company_id'    => 6000627186,
                                                        'custom_fields' => ['flag'          => false,
                                                                            #'ticket_type'   => 24000001493, #@$paramsArr['ticket_type'],
                                                                            #'cause'        => 'Hardware',#$paramsArr['cause'], #'NFF'
                                                                            #'group'        => 24000001493,
                                                                            #'type_it'       => 'User'
                                                                            #'product'       => @$paramsArr['product'], #'Support Request',
                                                                            ],
                                                        #'attachments'   => $paramsArr['attachments'],
                                                        #'cc_emails'     => @$paramsArr['cc_emails'], 
                                                    ]);
        $this->curl_options[CURLOPT_URL]            = $this->api_url .'tickets';
        #die ("<pre>New Ticket params:".print_r($paramsArr, true) ."</pre>");

        #$this->results = $this->doRequest();
        $this->doRequest();
        return $this;
    }



    /**
     * Method to set tickets URL and run request
     *
     * @param    none
     * @return   doRequest result (JSON string)
     */
    public function getTickets ($filters = NULL) {

        $this->filters  = '?include=company,requester,stats';
        if ( is_array($filters) && count($filters)  > 0)
        {
            $this->filters .= implode('&', $filters);
        }
        else if ($filters  != '') 
        {
            $this->filters .= '&'. $filters;
        }
        #$this->filters    .= '?query="'. urlencode("my.api@my-corp.com") .'"';
        $this->filters     .= '&order_by=status&order_type=desc&per_page=20&page=1';
        $this->filters     .= '&include=company,requester,stats';

        $this->curl_options[CURLOPT_POST] = FALSE;
        $this->curl_options[CURLOPT_URL]  = $this->api_url .'tickets'. $this->filters;

        $this->results = $this->doRequest()->results;

        return $this;
    }

    /**
     * Method to set tickets URL and run request
     *
     * @param    none
     * @return   doRequest result (JSON string)
     */
    public function getTicket($ticketID = 0) {

        if (!$ticketID > 0)
            return FALSE;

        $this->ticketID = $ticketID;

        $this->curl_options[CURLOPT_POST]       = FALSE;
        $this->curl_options[CURLOPT_URL]        = $this->api_url .'tickets/'. $this->ticketID .'?include=company,requester,conversations,stats';

        $this->doRequest();

        return $this;
    }

    /**
     * Method to add new comment on ticket
     *
     * @param    none
     * @return   doRequest result (JSON string)
     */
    public function addConversation($conversation = NULL) {
        if ($conversation == '')
            return FALSE;

        unset($this->curl_options[CURLOPT_HTTPHEADER]);
        $this->curl_options[CURLOPT_POST]           = TRUE;
        $this->curl_options[CURLOPT_CUSTOMREQUEST]  = 'POST';
        $this->curl_options[CURLOPT_POSTFIELDS]     = [
                                                    'body'       => $conversation,
                                                    'from_email' => 'email@my-corp.com'
                                                    ];
        $this->curl_options[CURLOPT_URL]        = $this->api_url .'tickets/'. $this->ticketID .'/reply';

        $this->results = $this->doRequest();

        return $this;
    }

    /**
     * Method to attach file to ticket
     *
     * @param    none
     * @return   doRequest result (JSON string)
     */
    public function addAttachment($attachment = NULL) {
        if (!$attachment > 0)
            return FALSE;

        unset($this->curl_options[CURLOPT_HTTPHEADER]);
        $this->curl_options[CURLOPT_POST]           = TRUE;
        $this->curl_options[CURLOPT_CUSTOMREQUEST]  = 'POST';
        $this->curl_options[CURLOPT_POSTFIELDS]     = [
                                                        'attachment' => $attachment,
                                                        'from_email' => 'email@my-corp.com'
                                                        ];

        #die ("ticketID #". $this->ticketID ."!attachment:$attachment<pre>curl options:". print_r($this->curl_options, true) ."</pre>");

        $this->results = $this->doRequest();

        return $this;
    }

    /**
     * Method to update status on ticket
     *
     * @param    none
     * @return   doRequest result (JSON string)
     */
    public function updateStatus($status = 0) {
        if (!$status > 0)
            return FALSE;

        $this->curl_options[CURLOPT_HTTPHEADER]     = ['Content-type: application/json'];
        $this->curl_options[CURLOPT_POST]           = TRUE;
        $this->curl_options[CURLOPT_CUSTOMREQUEST]  = 'PUT';
        $this->curl_options[CURLOPT_POSTFIELDS]     = json_encode(['status' => ($status*1)]);

        $this->results = $this->doRequest();

        return $this;
    }


    /**
     * Method to print formatted ticket info
     *
     * @param    none
     * @return   $this
     */
    public function showInfoCard() {

        echo "<pre>ticket Arr:". print_r($this->results, true) ."</pre>";

        return $this;
    }



    /**
     * Static method to show ticket status
     *
     * @param    int        $statusID
     * @return   var        string
     */
    public static function getTicketStatus($statusID = 0) {

        $statusTxt = '';
        switch ($statusID)
        {
            case 9:
                $statusTxt = 'WoC-NoClose';
                break;
            case 8:
                $statusTxt = 'undefined (8)';
                break;
            case 7:
                $statusTxt = 'Wo3rd Party';
                break;
            case 6:
                $statusTxt = 'WoC';
                break;
            case 5:
                $statusTxt = 'closed';
                break;
            case 4:
                $statusTxt = 'resolved';
                break;
            case 3:
                $statusTxt = 'pending';
                break;
            case 2:
                $statusTxt = 'open';
                break;
            case 1:
                $statusTxt = 'undefined (1)';
                break;
            default:
                $statusTxt = 'unknown';
        }

        return $statusTxt;
    }

    /**
     * Static method to show ticket source
     *
     * @param    int        $sourceID
     * @return   var        string
     */
    public static function getTicketSource($sourceID = 0) {

        $sourceTxt = '';
        switch ($sourceID)
        {
            case 1:
                $sourceTxt = "Email";
                break;
            case 2:
                $sourceTxt = "Portal";
                break;
            case 3:
                $sourceTxt = "Phone";
                break;
            case 4:
                $sourceTxt = "Forum";
                break;
            case 5:
                $sourceTxt = "Twitter";
                break;
            case 6:
                $sourceTxt = "Facebook";
                break;
            case 7:
                $sourceTxt = "Chat";
                break;
            case 8:
                $sourceTxt = "MobiHelp";
                break;
            case 9:
                $sourceTxt = "Feedback Widget";
                break;
            case 10:
                $sourceTxt = "Outbound Email";
                break;
            case 11:
                $sourceTxt = "Ecommerce";
                break;
            default:
                $sourceTxt = 'unknown';
        }

        return $sourceTxt;
    }



    /**
     * Static method to show ticket source
     *
     * @param    int        $sourceID
     * @return   var        string
     */
    public static function pretifyDateTime($dateStr = '', $format = 'd/m/Y') {

        if ($dateStr == '')
            return false;

        $datetime           = strtotime($dateStr);

        if (!$datetime)
        {
            $formatted_datetime = "unknown";#date($format);
        }
        else {
            $formatted_datetime = date($format, $datetime);
        }

        return $formatted_datetime;
    }



    /**
     * Destructor.
     *
     * @param    array    $config (auth_key & url)
     */
    public function __destruct () {

        if (isset($this->curl_handle))
            curl_close($this->curl_handle);
    }
}